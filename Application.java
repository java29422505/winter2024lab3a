import java.util.Scanner;
public class Application{
	public static void main (String[] args){

		Scanner reader= new Scanner(System.in);

		// make student1 object.
		Student student1 = new Student();

		System.out.println("Enter first student name");

		student1.name= reader.nextLine();

		System.out.println("Enter first Student's grade");

		student1.grade=Integer.parseInt(reader.nextLine());

		System.out.println("Enter first Student's course");

		student1.course= reader.nextLine();

		// calling instance methods from student class

		student1.study();
		student1.takeNotes();


		//  making second student object
		//Student student2 = new Student();


		// calling instance methods from students class here
		//student2.study();
		//student2.takeNotes();


		Student [] section3 = new Student [3];

		section3[0]=student1;

		// print out the field of the first student

		System.out.println("here is the field of the first student: "+ section3[0].name);
		System.out.println("You have got a grade of:" + section3[0].grade);
		System.out.println("Your course is: "+ section3[0].course);


		// checking the nullPointerException by printing the section3[2] since there is no 3rd student.

		
		//section3[1]=student2;

		// assigning student3 a new object

		section3[2]=new Student();

		// set student3 his values

		section3[2].name="narges";
		section3[2].grade= 75;
		section3[2].course="English";


		// print it all here.
		
		System.out.println("Here is the Error: " + section3[2].name);
		System.out.println("Here is your grade: "+section3[2].grade);
		System.out.println("Here is your course: "+section3[2].course);



	}
}